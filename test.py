
from __future__ import print_function
from apiclient import discovery
from httplib2 import Http
from oauth2client import file, client, tools
import logging, json

API_KEY = 'AIzaSyD_L1ieEWC3C9cVX_2niYVL0OmN5R-fsmw'
SPREADSHEET_ID = '1hLfT9LQ01JQIfRBPLoluey7aJorv-5FpDyOLmRtIlDM'
SCOPES = 'https://www.googleapis.com/auth/spreadsheets'

store = file.Storage('storage.json')
creds = store.get()
if not creds or creds.invalid:
    flow = client.flow_from_clientsecrets('client_secret.json', SCOPES)
    creds = tools.run_flow(flow, store)

SERVICE = discovery.build('sheets', 'v4', developerKey=API_KEY,http=creds.authorize(Http()))

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.DEBUG)
logger = logging.getLogger(__name__)

tab = 'Questions'
range_from = 'A'
range_to = 'E'
rangeName = '%s!%s:%s' % (tab, range_from, range_to)
result = SERVICE.spreadsheets().values().get(
    spreadsheetId=SPREADSHEET_ID, range=rangeName).execute()

data = result.get('values', [])


#print data['qlist'][0][1]
# len = len(data)
# chat_data = []
#
# for i in range(0, len):
#     if data[i][0].isdigit():
#         chat_data.append([])
#     chat_data[-1].append( data[i] )  # add row to question list
#
# for v in chat_data:
#     print v

def write_range(tab,range_from,range_to,data):
    rangeName = '%s!%s:%s' % (tab, range_from, range_to)
    result = SERVICE.spreadsheets().values().update(
        spreadsheetId=SPREADSHEET_ID, range=rangeName, body=data).execute()


    rangeName = '%s!%s:%s' % (tab, range_from, range_to)
    result = SERVICE.spreadsheets().values().get(
        spreadsheetId=SPREADSHEET_ID, range=rangeName).execute()

#

write_range('Answers', 'A1', 'B2', 'test')