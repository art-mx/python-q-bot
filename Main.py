# -*- coding: utf-8 -*-
#!/usr/bin/env python
from emoji import emojize

import time,sys,os, requests
from telegram.ext import MessageHandler, Filters
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, ReplyKeyboardMarkup, KeyboardButton, ForceReply, ReplyKeyboardRemove
from telegram.ext import Updater, CommandHandler, CallbackQueryHandler, ChosenInlineResultHandler
from apiclient import discovery
import logging, json

API_KEY = 'AIzaSyD_L1ieEWC3C9cVX_2niYVL0OmN5R-fsmw'
SPREADSHEET_ID = '1hLfT9LQ01JQIfRBPLoluey7aJorv-5FpDyOLmRtIlDM'
TELEGRAM_TOKEN = '486768131:AAFML2HZvHk8BiHcUXudq4sFdkiQiJ4dHIc'

SERVICE = discovery.build('sheets', 'v4', developerKey=API_KEY)
updater = Updater(token=TELEGRAM_TOKEN)


logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.ERROR)
logger = logging.getLogger(__name__)


def parse_message(bot,update):
    pass
#

def get_range(tab,range_from,range_to):
    rangeName = '%s!%s:%s' % (tab,range_from,range_to)
    result = SERVICE.spreadsheets().values().get(
        spreadsheetId=SPREADSHEET_ID, range=rangeName).execute()
    return result.get('values', [])
#
def write_range(tab,range_from,range_to,data):
    rangeName = '%s!%s:%s' % (tab, range_from, range_to)
    result = SERVICE.spreadsheets().values().update(
        spreadsheetId=SPREADSHEET_ID, range=rangeName, body=data).execute()
    print result

    rangeName = '%s!%s:%s' % (tab, range_from, range_to)
    result = SERVICE.spreadsheets().values().get(
        spreadsheetId=SPREADSHEET_ID, range=rangeName).execute()

#
def start(bot, update, chat_data):


    user_info = update.message.from_user
    lang = user_info['language_code']

    chat_data['user_id'] = user_info['id']
    chat_data['username'] = user_info['first_name']

    chat_data['q_row_range'] =  []
    chat_data['qpos'] = 0
    chat_data['score'] = {}
    chat_data['best_careers'] = {}
    chat_data['question_data'] = None
    data = get_range('Questions', 'A', 'E')

    qlen = len(data)
    for i in range(0, qlen):
        if data[i][0].isdigit():
            chat_data['q_row_range'].append(i)
    print chat_data['q_row_range']
        #chat_data['qlist'][-1].append(data[i])  # add row to question list


    next_question(bot,update,chat_data)
#
def button(bot, update, chat_data):

    query = update.callback_query
    chat_id = chat_data['user_id']

    print ("\n update: ", update.callback_query, '\n')
    print ("\n query: ", query.message, '\n')
    print ("query.data= ", query.data)
    print ("chat data= ", chat_data)

    bot.edit_message_reply_markup(chat_id=update.callback_query.message.chat_id,
                                 message_id=update.callback_query.message.message_id,
                                 reply_markup=None)
    #bot.send_message(chat_id, text=query.data)
    process_answer(bot, update, query.data, chat_data)
#
def process_answer(bot, update, query_data, chat_data):

    chat_id = chat_data['user_id']
    question_data = chat_data['question_data']
    print 'question data: ',question_data
    answer = question_data[int(query_data)]
    print 'answer: ', answer
    bot.send_message(chat_id, text='YOUR ANSWER: '+answer[2])

    if answer[3] =='end':
        answer = emojize(answer[4], use_aliases=True)
        bot.send_message(chat_id, text=answer)
        return
    elif answer[3] == 'add points':
        add_points(answer[4],chat_data)
        next_question(bot, update, chat_data)
    elif answer[3] == 'continue':
        next_question(bot, update, chat_data)
#
def add_points(answer_points,chat_data):
    print answer_points
    elem = answer_points.split(',')
    for e in elem:
        cat = e.replace(' ','')
        print cat[1:],cat[0]

        # if key exists, add point to it
        if cat[1:] in chat_data['score']:
            chat_data['score'][cat[1:]] += int(cat[0])
        # if not create and add points to it
        else: chat_data['score'][cat[1:]] = int(cat[0])

    print 'current score: ',chat_data['score']
#
def error(bot, update, error):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, error)
#
def next_question(bot,update,chat_data):

    print ("\n update: ", update, '\n')
    print 'qpos:',chat_data['qpos']
    chat_id = chat_data['user_id']
    i = chat_data['qpos']
    chat_data['qpos'] += 1
    q_range_from = 'A%s' % str(chat_data['q_row_range'][i]+1)
    try: #regular range
        q_range_to =  'E%s' % str(chat_data['q_row_range'][i+1])
        print 'exception in line 126'
    except: #in case this fails, select just one line
        q_range_to = 'E%s' % str(chat_data['q_row_range'][i]+1)
    print q_range_from,q_range_to
    question_data = get_range('Questions', q_range_from, q_range_to)
    chat_data['question_data'] = question_data
    print 'question data: ',question_data

    opt_num = len(question_data) - 1
    options_row = []
    for j in range(1, opt_num + 1):
        options_row.append(question_data[j])
    print 'opt row:', options_row

    ### V1 with options as buttons ###
    # if this is a question with options
    if question_data[0][1] == 'ask':
        ask_question(bot,chat_data,question_data,options_row)
    if question_data[0][1] == 'say':
        bot.send_message(chat_id, text=question_data[0][2])
        if question_data[0][3] == 'end':
            pass
        elif question_data[0][3] == 'continue':
            next_question(bot, update, chat_data)
    if question_data[0][1] == 'result':
        display_result(bot, update, chat_data, question_data,options_row)
    if question_data[0][1] == 'explain':
        explain_result(bot, update, chat_data, question_data,options_row)
#
def ask_question(bot,chat_data,question_data,options_row):
    chat_id = chat_data['user_id']
    # create a question with inline buttons
    buttons = []
    for option in options_row:
        button_name = option[1] + ") " + option[2]
        buttons.append([InlineKeyboardButton(button_name, callback_data=option[1])])

    reply_markup = InlineKeyboardMarkup(buttons)
    bot.send_message(chat_id, text=question_data[0][2], reply_markup=reply_markup)
#
def display_result(bot, update, chat_data,question_data,options_row):
    chat_id = chat_data['user_id']
    bot.send_message(chat_id, text=question_data[0][2])
    highest_score = 0

    for cat,score in chat_data['score'].items():
        highest_score = score if score > highest_score else highest_score
    for cat, score in chat_data['score'].items():
        if score == highest_score:
            chat_data['best_careers'][cat] = None # populate with ids of best careers
    for option in options_row:
        if option[2] in chat_data['best_careers']:
            chat_data['best_careers'][option[2]] = option[4]

            bot.send_message(chat_id, text=option[4])
            time.sleep(0.1)
    record_result(bot,update,chat_data)
    next_question(bot,update,chat_data)

#
def explain_result(bot, update, chat_data,question_data,options_row):
    chat_id = chat_data['user_id']
    bot.send_message(chat_id, text=question_data[0][2])
    for option in options_row:
        print 'option: ',option
        if option[2] in chat_data['best_careers']:
            print chat_data['best_careers'][option[2]]
            explanation = ('%s because: %s' % (chat_data['best_careers'][option[2]], option[4]))
            print explanation
            bot.send_message(chat_id, text=explanation)
    next_question(bot, update, chat_data)
#
def record_result(bot,update,chat_data):
    pass
#
# set up bot objects
dp = updater.dispatcher
# add handlers
dp.add_handler(MessageHandler(Filters.text, parse_message, pass_chat_data=True))
dp.add_handler(CommandHandler("start", start, pass_chat_data=True))
dp.add_handler(CallbackQueryHandler(button, pass_chat_data=True))

updater.dispatcher.add_error_handler(error)
updater.start_polling()
updater.idle()
